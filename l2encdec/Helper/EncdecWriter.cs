﻿using System.IO;
using System.Text;

namespace l2encdec.Helper
{
    public class EncdecWriter : BinaryWriter
    {
        public EncdecWriter(Stream input) : base(input) { }
        public EncdecWriter(Stream input, Encoding encoding) : base(input, encoding) { }
        public EncdecWriter(Stream input, Encoding encoding, bool leaveOpen) : base(input, encoding, leaveOpen) { }
    }
}
