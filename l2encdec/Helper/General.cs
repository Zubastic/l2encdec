﻿using System;

namespace l2encdec.Helper
{
    public static class General
    {
        public static byte[] Hex(string hex)
        {
            hex = hex.Replace(" ", "");
            var numberChars = hex.Length;
            var bytes = new byte[numberChars / 2];
            for (var i = 0; i < numberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public static string Hex(byte[] bytes)
        {
            return BitConverter.ToString(bytes).Replace("-", " ");
        }

        public static string Hex(byte bytes)
        {
            return BitConverter.ToString(new[] { bytes }).Replace("-", " ");
        }
    }
}
