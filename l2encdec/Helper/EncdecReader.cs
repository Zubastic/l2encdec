﻿using System.IO;
using System.Text;

namespace l2encdec.Helper
{
    public class EncdecReader : BinaryReader
    {
        public EncdecReader(Stream input) : base(input) {}
        public EncdecReader(Stream input, Encoding encoding) : base(input, encoding) {}
        public EncdecReader(Stream input, Encoding encoding, bool leaveOpen) : base(input, encoding, leaveOpen) {}
    }
}
