﻿using System;
using System.IO;
using System.Numerics;
using l2encdec.Blowfish;
using l2encdec.Helper;
using l2encdec.Lame;
using l2encdec.Rsa;
using l2encdec.Xor;

namespace l2encdec
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //args.Length > 0 ? args[0] : @"C:\Users\Zubastic\AppData\Roaming\Skype\zubastic32\chatsync"
        }

        public const int NO_CRYPT = -1;
        public static int HEADER_SIZE = 28;

        private static readonly BigInteger[,] RSA_KEYS =
        {
            {L2Ver41x.MODULUS_411, L2Ver41x.PRIVATE_EXPONENT_411},
            {L2Ver41x.MODULUS_412, L2Ver41x.PRIVATE_EXPONENT_412},
            {L2Ver41x.MODULUS_413, L2Ver41x.PRIVATE_EXPONENT_413},
            {L2Ver41x.MODULUS_414, L2Ver41x.PRIVATE_EXPONENT_414}
        };

        private static BigInteger _publicModulus = L2Ver41x.MODULUS_L2ENCDEC;
        private static BigInteger _publicExponent = L2Ver41x.PUBLIC_EXPONENT_L2ENCDEC;

        public static void set41xPrivateKey(int version, BigInteger modulus, BigInteger exponent)
        {
            RSA_KEYS[version - 411, 0] = modulus;
            RSA_KEYS[version - 411, 1] = exponent;
        }

        public static void set41xPublicKey(BigInteger publicModulus, BigInteger publicExponent)
        {
            _publicModulus = publicModulus;
            _publicExponent = publicExponent;
        }

        public static int ReadHeader(Stream input)
        {
            var header = new byte[HEADER_SIZE];
            input.Read(header, 0, HEADER_SIZE);
            Array.Reverse(header);
            var headerStr = BitConverter.ToString(header);
            return !headerStr.StartsWith("Lineage2Ver") ? NO_CRYPT : int.Parse(headerStr.Replace("Lineage2Ver", ""));
        }

        public static void WriteHeader(Stream output, int version)
        {
            var strVer = General.Hex("Lineage2Ver" + version);
            Array.Reverse(strVer); //reverse array, check it
            output.Write(strVer, 0, strVer.Length);
        }

        public static EncdecReader Decrypt(Stream input, string fileName)
        {
            var version = ReadHeader(input);
            BigInteger modulus;
            BigInteger exponent;
            switch (version)
            {
                case NO_CRYPT:
                    return new EncdecReader(input);
                //XOR
                case 811:
                case 821:
                    input = LameCrypt.wrapInput(input);
                    version -= 700;
                    return new L2Ver1x1BinaryReader(input, version == 111 ? L2Ver1x1.XOR_KEY_111 : L2Ver1x1.getXORKey121(fileName));
                case 111:
                case 121:
                    return new L2Ver1x1BinaryReader(input, version == 111 ? L2Ver1x1.XOR_KEY_111 : L2Ver1x1.getXORKey121(fileName));
                case 820:
                    input = LameCrypt.wrapInput(input);
                    return new L2Ver120BinaryReader(input);
                case 120:
                    return new L2Ver120BinaryReader(input);
                //BLOWFISH
                case 911:
                case 912:
                    input = LameCrypt.wrapInput(input);
                    version -= 700;
                    return new L2Ver21xBinaryReader(input, version == 211 ? L2Ver21x.BLOWFISH_KEY_211 : L2Ver21x.BLOWFISH_KEY_212);
                case 211:
                case 212:
                    return new L2Ver21xBinaryReader(input, version == 211 ? L2Ver21x.BLOWFISH_KEY_211 : L2Ver21x.BLOWFISH_KEY_212);
                //RSA
                case 611:
                case 612:
                case 613:
                case 614:
                    input = LameCrypt.wrapInput(input);
                    version -= 200;
                    modulus = RSA_KEYS[version - 41, 0];
                    exponent = RSA_KEYS[version - 411, 1];
                    return new L2Ver41xBinaryReader(input, modulus, exponent);
                case 411:
                case 412:
                case 413:
                case 414:
                    modulus = RSA_KEYS[version - 41, 0];
                    exponent = RSA_KEYS[version - 411, 1];
                    return new L2Ver41xBinaryReader(input, modulus, exponent);
                default:
                    throw new ApplicationException("Unsupported crypt version: " + version);
            }
        }

        public static EncdecWriter Encrypt(Stream output, string fileName, int version)
        {
            if (version == NO_CRYPT)
                return new EncdecWriter(output);

            WriteHeader(output, version);
            switch (version)
            {
                //XOR
                case 811:
                case 821:
                    output = LameCrypt.wrapOutput(output);
                    version -= 700;
                    return new L2Ver1x1BinaryWriter(output, version == 111 ? L2Ver1x1.XOR_KEY_111 : L2Ver1x1.getXORKey121(fileName));
                case 111:
                case 121:
                    return new L2Ver1x1BinaryWriter(output, version == 111 ? L2Ver1x1.XOR_KEY_111 : L2Ver1x1.getXORKey121(fileName));
                case 820:
                    output = LameCrypt.wrapOutput(output);
                    return new L2Ver120BinaryWriter(output);
                case 120:
                    return new L2Ver120BinaryWriter(output);
                //BLOWFISH
                case 911:
                case 912:
                    output = LameCrypt.wrapOutput(output);
                    version -= 700;
                    return new L2Ver21xBinaryWriter(output, version == 211 ? L2Ver21x.BLOWFISH_KEY_211 : L2Ver21x.BLOWFISH_KEY_212);
                case 211:
                case 212:
                    return new L2Ver21xBinaryWriter(output, version == 211 ? L2Ver21x.BLOWFISH_KEY_211 : L2Ver21x.BLOWFISH_KEY_212);
                //RSA
                case 611:
                case 612:
                case 613:
                case 614:
                    output = LameCrypt.wrapOutput(output);
                    return new L2Ver41xBinaryWriter(output, _publicModulus, _publicExponent);
                case 411:
                case 412:
                case 413:
                case 414:
                    return new L2Ver41xBinaryWriter(output, _publicModulus, _publicExponent);
                default:
                    throw new ApplicationException("Unsupported version: " + version);
            }
        }
    }
}
