﻿using l2encdec.Helper;

namespace l2encdec.Blowfish
{
    public class L2Ver21x
    {
        public static byte[] BLOWFISH_KEY_211 = General.Hex("31==-%&@!^+][;'.]94-\0");
        public static byte[] BLOWFISH_KEY_212 = General.Hex("[;'.]94-&@%!^+]-31==\0");
    }
}
