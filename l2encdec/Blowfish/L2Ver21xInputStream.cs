﻿using System.IO;
using System.Security;
using System.Security.Cryptography;
using l2encdec.Helper;

namespace l2encdec.Blowfish
{
    public class L2Ver21xBinaryReader : EncdecReader
    {
        private BlowfishEngine blowfish = new BlowfishEngine();
        private readonly byte[] _dataBuffer = new byte[8];
        private int _pos;
        public L2Ver21xBinaryReader(Stream input, byte[] key) : base(input)
        {
            blowfish.init(false, key);
        }

        public override int Read()
        {
            if (base.BaseStream.Position == base.BaseStream.Length)
            {
                var readBuffer = new byte[BaseStream.Length];
                ReadFully(readBuffer);
                for (var i = 0; i < 8; i++) _dataBuffer[i] = 0x00;
                try
                {
                    blowfish.ProcessBlock(readBuffer, 0, _dataBuffer, _pos);
                }
                catch (SecurityException e)
                {
                    throw new CryptographicException(e.HResult);
                }
            }
            return _dataBuffer[_pos++] & 0xff;
        }

        private void ReadFully(byte[] buffer)
        {
            var offset = 0;
            int readBytes;
            do
            {
                // If you are using Socket directly instead of a Stream:
                //readBytes = socket.Receive(buffer, offset, buffer.Length - offset,
                //                           SocketFlags.None);

                readBytes = base.BaseStream.Read(buffer, offset, buffer.Length - offset);
                offset += readBytes;
            } while (readBytes > 0 && offset < buffer.Length);

            if (offset < buffer.Length)
            {
                throw new EndOfStreamException();
            }
        }
    }
}
