﻿using System;
using System.IO;
using System.Security.Cryptography;
using l2encdec.Helper;

namespace l2encdec.Blowfish
{
    public class L2Ver21xBinaryWriter : EncdecWriter
    {
        private readonly BlowfishEngine _blowfish = new BlowfishEngine();

        private readonly byte[] _writeBuffer = new byte[8];
        private readonly byte[] _dataBuffer = new byte[8];
        private int _pos;

        public L2Ver21xBinaryWriter(Stream input, byte[] key) : base(input)
        {
            _blowfish.init(true, key);
        }

        public override void Write(int b)
        {
            _dataBuffer[_pos++] = (byte)b;
            if (_pos != _dataBuffer.Length) return;
            WriteData();
            for (var i = 0; i < 8; i++) _dataBuffer[i] = 0x00;
        }

        private void WriteData()
        {
            if (_pos == 0)
                return;

            for (var i = _pos; i < _dataBuffer.Length; i++) _dataBuffer[i] = 0x00;
            try
            {
                _blowfish.ProcessBlock(_dataBuffer, _pos, _writeBuffer, 0);
            }
            catch (InvalidOperationException e)
            {
                throw new CryptographicException(e.Message);
            }
            base.Write(_writeBuffer);
        }
    }
}
