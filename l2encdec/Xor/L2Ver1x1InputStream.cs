﻿using System.IO;
using l2encdec.Helper;

namespace l2encdec.Xor
{
    public class L2Ver1x1BinaryReader : EncdecReader
    {
        private int xorKey;
    
        public L2Ver1x1BinaryReader(Stream input, int xorKey) : base(input)
        {
            this.xorKey = xorKey;
        }
    
        public override int Read()
        {
            int b = base.Read();
            return b < 0 ? b : b ^ xorKey;
        }
    
        public override int Read(byte[] b, int off, int len)
        {
            int r = base.Read(b, off, len);
            for (int i = 0; i < r; i++)
                b[off + i] ^= (byte)xorKey;
            return r;
        }
    }
}
