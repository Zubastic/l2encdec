﻿using System.IO;
using l2encdec.Helper;

namespace l2encdec.Xor
{
    public class L2Ver120BinaryReader : EncdecReader
    {
        private int _ind = L2Ver120.START_IND;

        public L2Ver120BinaryReader(Stream input) : base(input) {}

        public override int Read()
        {
            var b = base.Read();
            return b < 0 ? b : b ^ L2Ver120.getXORKey(_ind++);
        }

        public override int Read(byte[] b, int off, int len)
        {
            var r = base.Read(b, off, len);
            for (var i = 0; i < r; i++)
                b[off + i] ^= (byte)L2Ver120.getXORKey(_ind++);
            return r;
        }
    }
}