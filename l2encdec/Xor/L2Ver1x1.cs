﻿using System.Linq;

namespace l2encdec.Xor
{
    public class L2Ver1x1
    {
        public static int XOR_KEY_111 = 0xAC;

        public static int getXORKey121(string filename)
        {
            filename = filename.ToLowerInvariant();
            var ind = filename.Aggregate(0, (current, t) => current + t);
            return ind & 0xff;
        }
    }
}
