﻿using System.IO;
using System.Text;
using l2encdec.Helper;

namespace l2encdec.Xor
{
    public class L2Ver120BinaryWriter : EncdecWriter
    {
        public L2Ver120BinaryWriter(Stream input) : base(input) { }
        public L2Ver120BinaryWriter(Stream input, Encoding encoding) : base(input, encoding) { }
        public L2Ver120BinaryWriter(Stream input, Encoding encoding, bool leaveOpen) : base(input, encoding, leaveOpen) { }
        private int ind = L2Ver120.START_IND;

        public override void Write(int b)
        {
            base.Write(b ^ L2Ver120.getXORKey(ind++));
        }
    }
}
