﻿namespace l2encdec.Xor
{
    public static class L2Ver120
    {
        public static int START_IND = 0xE6;

        public static int getXORKey(int n)
        {
            var d1 = n & 0xf;
            var d2 = (n >> 4) & 0xf;
            var d3 = (n >> 8) & 0xf;
            var d4 = (n >> 12) & 0xf;
            return ((d2 ^ d4) << 4) | (d1 ^ d3);
        }
    }
}
