﻿using System.IO;
using l2encdec.Helper;

namespace l2encdec.Xor
{
    public class L2Ver1x1BinaryWriter : EncdecWriter
    {
        private int xorKey;
    
        public L2Ver1x1BinaryWriter(Stream output, int xorKey) : base(output)
        {
            this.xorKey = xorKey;
        }
    
        public override void Write(int b)
        {
            base.Write(b ^ xorKey);
        }
    }
}
