﻿using System.IO;
using System.Text;
using l2encdec.Helper;

namespace l2encdec.Lame
{
    public static class LameCrypt
    {
        public static string SecretString = "Range check error while converting variant of type (%s) into type (%s)";

        public static Stream wrapInput(Stream stream)
        {
            return new LameCryptReader(stream).BaseStream;
        }

        public static Stream wrapOutput(Stream stream)
        {
            return new LameCryptWriter(stream).BaseStream;
        }
    }

    public class LameCryptReader : EncdecReader
    {
        private static int _pos = Program.HEADER_SIZE % LameCrypt.SecretString.Length;
        public LameCryptReader(Stream input) : base(input) { }
        public LameCryptReader(Stream input, Encoding encoding) : base(input, encoding) { }
        public LameCryptReader(Stream input, Encoding encoding, bool leaveOpen) : base(input, encoding, leaveOpen) { }

        public override int Read()
        {
            var b = BaseStream.ReadByte();
            if (b == -1)
            {
                return -1;
            }

            b ^= LameCrypt.SecretString[_pos++];

            if (_pos == LameCrypt.SecretString.Length)
                _pos = 0;

            return b;
        }
    }

    public class LameCryptWriter : EncdecWriter
    {
        private static int _pos = Program.HEADER_SIZE % LameCrypt.SecretString.Length;
        public LameCryptWriter(Stream input) : base(input) { }
        public LameCryptWriter(Stream input, Encoding encoding) : base(input, encoding) { }
        public LameCryptWriter(Stream input, Encoding encoding, bool leaveOpen) : base(input, encoding, leaveOpen) { }

        public override void Write(int b)
        {
            b ^= LameCrypt.SecretString[_pos++];

            if (_pos == LameCrypt.SecretString.Length)
                _pos = 0;

            base.Write(b);
        }
    }
}
