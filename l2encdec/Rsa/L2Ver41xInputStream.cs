﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Numerics;
using l2encdec.Helper;

namespace l2encdec.Rsa
{
    public class L2Ver41xBinaryReader : EncdecReader
    {
        private BigInteger _exponent;
        private BigInteger _modulus;
        private long _blockCount;

        public L2Ver41xBinaryReader(Stream input) : base(input) {}

        public L2Ver41xBinaryReader(Stream input, BigInteger modulus, BigInteger exponent) : this(input)
        {
            this._modulus = modulus;
            this._exponent = exponent;
            _blockCount = (input.Length - 28) / 128; //calc length
        }

        public new byte[] Read()
        {
            var arr = new List<byte>();
            for (var i = 0; i < _blockCount; i++)
            {
                var block = base.ReadBytes(128);
                var decode = BigInteger.ModPow(new BigInteger(block), _exponent, _modulus).ToByteArray();
                //todo check for 250 length
                var size = (int)decode[0];
                if (decode.Length - 1 != size)
                {
                    new ApplicationException();
                }
                if (decode.Length != 124)
                {
                    var len = decode.Length - size;
                    while (len > 2 && decode[len - 1] != '\0')
                    {
                        len--;
                    }
                    for (var j = len; j < size; j++)
                    {
                        arr.Add(decode[j]);   
                    }
                }
                else
                {
                    for (var j = decode.Length-size; j < decode.Length; j++)
                    {
                        arr.Add(decode[j]);
                    }
                }
            }
            byte[] buf = new byte[1024];
            using (MemoryStream output = new MemoryStream())
            {
                // Decompress the contents of the input file
                using (var inp = new DeflateStream(BaseStream, CompressionMode.Decompress))
                {
                    int len;
                    while ((len = inp.Read(buf, 0, buf.Length)) > 0)
                    {
                        // Write the data block to the decompressed output stream
                        output.Write(buf, 0, len);
                    }
                }
                return output.ToArray();
            }
        }
    }
}